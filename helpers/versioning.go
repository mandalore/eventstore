package helpers

// Versioning ...
type Versioning struct {
	version int
}

// GetVersion ...
func (v *Versioning) GetVersion() int {
	return v.version
}

// SetVersion ...
func (v *Versioning) SetVersion(version int) {
	v.version = version
}

// IncrementVersion ...
func (v *Versioning) IncrementVersion(version int) {
	v.version = v.version + 1
}
