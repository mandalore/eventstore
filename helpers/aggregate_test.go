package helpers

import "testing"

type mockEvent struct{}

func (m *mockEvent) GetType() string { return "event" }

func TestAggregate(t *testing.T) {
	mock := Aggregate{}

	id := mock.GetID()
	if id != "" {
		t.Error("expected \"\", got", id)
	}

	mock.ID = "myID"

	id = mock.GetID()
	if id != "myID" {
		t.Error("expected \"myID\", got", id)
	}

	atype := mock.GetType()
	if atype != "" {
		t.Error("expected \"\", got", atype)
	}

	mock.Type = "myType"

	atype = mock.GetType()
	if atype != "myType" {
		t.Error("expected \"myType\", got", atype)
	}

	version := mock.GetVersion()
	if version != 0 {
		t.Error("expected 0, got", version)
	}

	mock.SetVersion(12)

	version = mock.GetVersion()
	if version != 12 {
		t.Error("expected 12, got", version)
	}

}

func TestAggregateHelperUncommited(t *testing.T) {
	mock := &Aggregate{}

	length := len(mock.Log())
	if length != 0 {
		t.Error("expected 0, got", length)
	}

	mock.Raise(&mockEvent{})

	length = len(mock.Log())
	if length == 0 {
		t.Error("expected 1, got", length)
	}

	mock.ResetLog()

	length = len(mock.Log())
	if length != 0 {
		t.Error("expected 0, got", length)
	}
}
