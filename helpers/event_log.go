package helpers

import "gitlab.com/mandalore/eventstore/domain"

// EventLog Composable type that can be used to create Aggregates
type EventLog struct {
	pending []domain.Event
}

// Raise ...
func (log *EventLog) Raise(event domain.Event) {
	if log.pending == nil {
		log.ResetLog()
	}

	log.pending = append(log.pending, event)
}

// Log ...
func (log *EventLog) Log() []domain.Event {
	return log.pending
}

// ResetLog the events
func (log *EventLog) ResetLog() {
	log.pending = []domain.Event{}
}
