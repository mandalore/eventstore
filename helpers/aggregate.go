package helpers

import "gitlab.com/mandalore/eventstore/domain"

// Aggregate Base Aggregate ...
type Aggregate struct {
	ID   string
	Type string
	EventLog
	Versioning
}

// NewAggregate Creates a Helper Aggregate
func NewAggregate(aType string, id string) *Aggregate {
	return &Aggregate{ID: id, Type: aType}
}

// GetID required by Domain.IIdentifiable interface
func (aggregate *Aggregate) GetID() string {
	return aggregate.ID
}

// GetType required by Domain.IIdentifiable interface
func (aggregate *Aggregate) GetType() string {
	return aggregate.Type
}

// Causes ...
func Causes(aggregate domain.EventCauser, event domain.Event) {
	aggregate.Apply(event)
	aggregate.Raise(event)
}
