package domain

type eventRaiser interface {
	Raise(Event)
}

type eventApplier interface {
	Apply(event Event) error
}

type identityGetter interface {
	GetID() string
	GetType() string
}

// VersionGetter ...
type VersionGetter interface {
	GetVersion() int
}

// VersionSetter ...
type VersionSetter interface {
	SetVersion(int)
}

// EventLogger ...
type EventLogger interface {
	eventRaiser
	Log() []Event
	ResetLog()
}

// EventCauser ...
type EventCauser interface {
	eventApplier
	eventRaiser
}

// IAggregateSerializer ...
type IAggregateSerializer interface {
	UnmarshalAggregate(*SnapshotWrapper, VersionSetter) error
	MarshalAggregate(VersionGetter) (*SnapshotWrapper, error)
	MarshalEvent(Event) (*EventWrapper, error)
	UnmarshalEvent(*EventWrapper) (Event, error)
}

// Event ...
type Event interface {
	GetType() string
}

// IdentityVersionGetter ...
type IdentityVersionGetter interface {
	identityGetter
	VersionGetter
}

// Storable ...
type Storable interface {
	EventLogger
	IdentityVersionGetter
	VersionSetter
}

// Snapshotable ...
type Snapshotable interface {
	EventLogger
	IdentityVersionGetter
}

// Hydratable ...
type Hydratable interface {
	VersionSetter
	IdentityVersionGetter
	eventApplier
}
