package domain

// SnapshotWrapper ...
type SnapshotWrapper struct {
	Version       int
	Serialization string
	Payload       []byte
}

// EventWrapper ... Add Aggregate Info it is usefull for matchiing
type EventWrapper struct {
	ID            string
	Version       int
	Type          string
	Serialization string
	Payload       []byte
}
