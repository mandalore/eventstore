#!/bin/bash

mkdir -p ./data
rm ./data/*

echo "mode: count" > ./data/coverage.out

for pkg in $(go list ./... | grep -v -E "vendor" | grep -v -E "bin"); do
        if ! go test -tags=integration -coverprofile ./data/coverage.out.tmp $pkg; then
                echo "Failed to run tests"
                exit 1
        fi
        if [ -f ./data/coverage.out.tmp ]; then
                tail -n +2 ./data/coverage.out.tmp >> ./data/coverage.out
                rm ./data/coverage.out.tmp
        fi
done

go tool cover -html=./data/coverage.out -o ./coverage.html
