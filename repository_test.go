package eventstore

import (
	"reflect"
	"testing"

	"gitlab.com/mandalore/eventstore/domain"
	"gitlab.com/mandalore/eventstore/mocks"
	"gitlab.com/mandalore/eventstore/storage/drivers"
)

type icomparable interface {
	domain.EventLogger
	domain.IdentityVersionGetter
}

func compareAggregate(expected icomparable, actual icomparable, t *testing.T) {
	var eID, eType string = expected.GetID(), expected.GetType()
	var aID, aType string = actual.GetID(), actual.GetType()

	if eID != aID {
		t.Error("Expected ID does not match Actual, expected ", eID, "!=", aID)
	}

	if eType != aType {
		t.Error("Expected Type does not match Actual, expected ", eType, "!=", aType)
	}

	if expected.GetVersion() != actual.GetVersion() {
		t.Error("Expected Version does not match Actual, expected ", expected.GetVersion(), "!=", actual.GetVersion())
	}

	if len(expected.Log()) != len(actual.Log()) {
		t.Error("Expected uncommited events are not the same size as actual, expected ", len(expected.Log()), "!=", len(actual.Log()))
	}

	expectedUncommited := expected.Log()
	actualUncommited := expected.Log()

	for i, sourceEvent := range expectedUncommited {
		if !reflect.DeepEqual(sourceEvent, actualUncommited[i]) {
			t.Error("Event ", i, "Not matching")
		}
	}
}

func TestStoreMeta(t *testing.T) {
	storage := drivers.NewInMemory()
	irepository := NewRepository(storage)
	irepository.RegisterSerializer("testing", &mocks.Serializer{})

	toFetchAggr := mocks.NewAggregate("testing", "22")
	toStoreAggr := mocks.NewAggregate("testing", "22")

	err := irepository.Hydrate(toFetchAggr)
	if err == nil || err.Error() != "Not Found" {
		t.Error("Getting By ID for non-existant aggregate, did not return error", err.Error())
	}

	err = irepository.Store(toStoreAggr)
	if err != nil {
		t.Error("Could not persist initial aggregate " + err.Error())
	}

	expected := mocks.NewAggregate("testing", "22")

	err = irepository.Hydrate(expected)
	if err != nil {
		t.Error("Getting By ID on existant aggregate, returned error " + err.Error())
	}

	compareAggregate(expected, toStoreAggr, t)
}

func TestStoreEvents(t *testing.T) {
	storage := drivers.NewInMemory()
	irepository := NewRepository(storage)
	irepository.RegisterSerializer("testing", &mocks.Serializer{})

	toStoreAggr := mocks.NewAggregate("testing", "22")
	toStoreAggr.Causes(&mocks.AggregateCreated{Name: "ZIS IS MY NAME"})

	expected := mocks.NewAggregate("testing", "22")

	err := irepository.Hydrate(expected)
	if err == nil || err.Error() != "Not Found" {
		t.Error("Getting By ID for non-existant aggregate, did not return error")
	}

	err = irepository.Store(toStoreAggr)

	if err != nil {
		t.Error("Could not persist initial aggregate " + err.Error())
	}

	if toStoreAggr.GetVersion() != 1 {
		t.Error("Incorrect Version stored, expected 1")
	}

	if len(toStoreAggr.Log()) != 0 {
		t.Error("Uncommitted Events were not cleared")
	}

	expected = mocks.NewAggregate("testing", "22")

	err = irepository.Hydrate(expected)
	if err != nil {
		t.Error("Getting By ID for non-existant aggregate, did not return error " + err.Error())
	}

	compareAggregate(expected, toStoreAggr, t)

	toStoreAggr = mocks.NewAggregate("testing", "22")
	toStoreAggr.SetVersion(0)
	toStoreAggr.Causes(&mocks.AggregateCreated{Name: "ZIS IS MY NAME"})

	err = irepository.Store(toStoreAggr)
	if err == nil {
		t.Error("Persisted out of sync aggregate events " + err.Error())
	}

	if len(toStoreAggr.Log()) != 1 {
		t.Error("Flushed Uncommitted events without actually storing them")
	}

	if toStoreAggr.GetVersion() != 0 {
		t.Error("Incremented Aggregate Version without actually storing it")
	}

	toStoreAggr = mocks.NewAggregate("testing", "22")
	err = irepository.Hydrate(toStoreAggr)
	if err != nil {
		t.Error("Getting By ID for non-existant aggregate, did not return error " + err.Error())
	}

	toStoreAggr.SetVersion(1)

	toStoreAggr.Causes(&mocks.AggregateBalanceIncremented{
		Amount: 100,
	})
	toStoreAggr.Causes(&mocks.AggregateBalanceDecremented{
		Amount: 50,
	})

	toStoreAggr.Causes(&mocks.AggregateBalanceIncremented{
		Amount: 20,
	})

	err = irepository.Store(toStoreAggr)
	if err != nil {
		t.Error("Failed to Store Aggregate" + err.Error())
	}

	if len(toStoreAggr.Log()) != 0 {
		t.Error("Failed Flushed Uncommitted events without actually storing them")
	}

	if toStoreAggr.GetVersion() != 4 {
		t.Error("Version not correctly set")
	}
}
