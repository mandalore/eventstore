package mocks

import (
	"encoding/json"
	"fmt"

	"gitlab.com/mandalore/eventstore/domain"
)

// Serializer ...
type Serializer struct{}

// MarshalEvent ...
func (serializer *Serializer) MarshalEvent(event domain.Event) (*domain.EventWrapper, error) {
	raw, err := json.Marshal(event)
	if err != nil {
		return nil, err
	}

	return &domain.EventWrapper{
		Type:          event.GetType(),
		Serialization: "json",
		Payload:       raw,
	}, nil
}

// UnmarshalEvent ...
func (serializer *Serializer) UnmarshalEvent(raw *domain.EventWrapper) (domain.Event, error) {
	var event domain.Event

	switch raw.Type {
	case "AggregateCreated":
		event = &AggregateCreated{}
		break
	case "AggregateBalanceIncremented":
		event = &AggregateBalanceIncremented{}
		break
	case "AggregateBalanceDecremented":
		event = &AggregateBalanceDecremented{}
		break
	default:
		return nil, fmt.Errorf("Unknown event type %s", raw.Type)
	}

	if err := json.Unmarshal(raw.Payload, event); err != nil {
		return nil, err
	}

	return event, nil
}

// UnmarshalAggregate ...
func (serializer *Serializer) UnmarshalAggregate(snapshot *domain.SnapshotWrapper, aggr domain.VersionSetter) error {
	if err := json.Unmarshal(snapshot.Payload, aggr); err != nil {
		return err
	}

	aggr.SetVersion(snapshot.Version)

	return nil
}

// MarshalAggregate ...
func (serializer *Serializer) MarshalAggregate(aggregate domain.VersionGetter) (*domain.SnapshotWrapper, error) {
	data, err := json.Marshal(aggregate)
	if err != nil {
		return nil, err
	}

	return &domain.SnapshotWrapper{
		Version:       aggregate.GetVersion(),
		Serialization: "json",
		Payload:       data,
	}, nil
}
