package mocks

import (
	"fmt"

	"gitlab.com/mandalore/eventstore/domain"
	"gitlab.com/mandalore/eventstore/helpers"
)

type AggregateCreated struct {
	Name string
}

func (e *AggregateCreated) GetType() string {
	return "AggregateCreated"
}

type AggregateBalanceIncremented struct {
	Amount int
}

func (e *AggregateBalanceIncremented) GetType() string {
	return "AggregateBalanceIncremented"
}

type AggregateBalanceDecremented struct {
	Amount int
}

func (e *AggregateBalanceDecremented) GetType() string {
	return "AggregateBalanceDecremented"
}

// Aggregate Mock Aggregate
type Aggregate struct {
	helpers.Aggregate
	Name    string `json:"name"`
	Balance int    `json:"balance"`
}

// NewAggregate Creates a New Mock Aggregate
func NewAggregate(atype string, id string) *Aggregate {
	return &Aggregate{
		Aggregate: helpers.Aggregate{
			ID:   id,
			Type: atype,
		},
	}
}

// Apply Implements IApplies Event
func (a *Aggregate) Apply(event domain.Event) error {
	switch e := event.(type) {
	case *AggregateCreated:
		a.Name = e.Name
		break
	case *AggregateBalanceDecremented:
		a.Balance -= e.Amount
		break
	case *AggregateBalanceIncremented:
		a.Balance += e.Amount
		break
	default:
		return fmt.Errorf("Cannot handle event %T", e)
	}

	return nil
}

// Causes ...
func (aggregate *Aggregate) Causes(event domain.Event) {
	helpers.Causes(aggregate, event)
}
