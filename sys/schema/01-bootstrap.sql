DROP SCHEMA IF EXISTS eventstore CASCADE;
CREATE SCHEMA eventstore;

SET search_path TO eventstore, public;

--CREATE OR REPLACE FUNCTION update_updated_at()
--RETURNS TRIGGER AS $$
--BEGIN
--   NEW.updated_at = now();
--   RETURN NEW;
--END;
--$$ language 'plpgsql';

CREATE TABLE serialization_types (
  serialization_type_id   SMALLSERIAL NOT NULL,
  slug                    TEXT NOT NULL,
  PRIMARY KEY(serialization_type_id),
  UNIQUE(slug)
);

INSERT INTO serialization_types VALUES(nextval('serialization_types_serialization_type_id_seq'),'json');

CREATE TABLE aggregate_types (
  aggregate_type_id   SERIAL NOT NULL,
  slug                TEXT NOT NULL,
  PRIMARY KEY(aggregate_type_id),
  UNIQUE(slug)
);

CREATE TABLE event_types (
  event_type_id       SERIAL NOT NULL,
  slug                TEXT NOT NULL,
  PRIMARY KEY(event_type_id),
  UNIQUE(event_type_id, slug)
);

CREATE TABLE aggregates (
  aggregate_id        UUID NOT NULL,
  aggregate_type_id   INT NOT NULL,
  aggregate_version   INT NOT NULL,
  PRIMARY KEY(aggregate_id, aggregate_type_id),
  FOREIGN KEY(aggregate_type_id) 
    REFERENCES aggregate_types(aggregate_type_id)
);

CREATE TABLE snapshots (
  aggregate_id           UUID NOT NULL,
  aggregate_type_id      INT NOT NULL,
  aggregate_version      INT NOT NULL,
  serialization_type_id  SMALLINT NOT NULL,
  data                   BYTEA NOT NULL,
  created_at             TIMESTAMP DEFAULT NOW(),
  updated_at             TIMESTAMP DEFAULT NOW(),
  PRIMARY KEY(aggregate_id, aggregate_type_id),
  FOREIGN KEY(aggregate_id, aggregate_type_id) 
    REFERENCES aggregates(aggregate_id, aggregate_type_id) INITIALLY DEFERRED,
  FOREIGN KEY(serialization_type_id) 
    REFERENCES serialization_types(serialization_type_id) INITIALLY DEFERRED  
);

--CREATE TRIGGER snapshots_updated_at BEFORE UPDATE
--  ON snapshots FOR EACH ROW EXECUTE PROCEDURE update_updated_at();

CREATE TABLE events (
  aggregate_id           UUID NOT NULL,
  aggregate_type_id      INT NOT NULL,
  aggregate_version      INT NOT NULL,

  event_id               UUID NOT NULL,
  event_type_id          INT NOT NULL,
  serialization_type_id  SMALLINT NOT NULL,

  correlation_id         UUID DEFAULT NULL,

  data                   BYTEA NOT NULL,
  is_dispatched          BOOLEAN NOT NULL DEFAULT 'f',
  created_at             TIMESTAMP DEFAULT NOW(),

  PRIMARY KEY(aggregate_id, aggregate_type_id, aggregate_version),
  UNIQUE(event_id),
  FOREIGN KEY(aggregate_id, aggregate_type_id) 
    REFERENCES aggregates(aggregate_id, aggregate_type_id) INITIALLY DEFERRED,
  FOREIGN KEY(event_type_id) 
    REFERENCES event_types(event_type_id) INITIALLY DEFERRED,
  FOREIGN KEY(serialization_type_id) 
    REFERENCES serialization_types(serialization_type_id) INITIALLY DEFERRED 
);