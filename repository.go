package eventstore

import (
	"errors"
	"sync"

	"gitlab.com/mandalore/eventstore/domain"
	"gitlab.com/mandalore/eventstore/storage"
)

// Repository ...
type Repository struct {
	mu          sync.RWMutex
	storage     storage.IStorage
	serializers map[string]domain.IAggregateSerializer
}

// NewRepository creates a new SQL ...
func NewRepository(storage storage.IStorage) *Repository {
	return &Repository{
		storage:     storage,
		serializers: map[string]domain.IAggregateSerializer{},
	}
}

// RegisterSerializer ...
func (repository *Repository) RegisterSerializer(aggregateType string, serializer domain.IAggregateSerializer) {
	repository.mu.Lock()
	defer repository.mu.Unlock()

	repository.serializers[aggregateType] = serializer
}

func (repository *Repository) serializerFor(aggregateType string) (domain.IAggregateSerializer, error) {
	repository.mu.RLock()
	defer repository.mu.RUnlock()

	if serializer, found := repository.serializers[aggregateType]; found {
		return serializer, nil
	}

	return nil, errors.New("No Serializer for " + aggregateType)
}

func validateAggregate(aggregate domain.IdentityVersionGetter, withVersion bool) error {
	if aggregate.GetID() == "" {
		return errors.New("Zero valued ID")
	}

	if aggregate.GetType() == "" {
		return errors.New("Zero valued Type")
	}

	if withVersion {
		if aggregate.GetVersion() != 0 {
			return errors.New("Invalid target Aggregate")
		}
	}

	return nil
}

// Hydrate ...
func (repository *Repository) Hydrate(aggregate domain.Hydratable) error {
	if err := validateAggregate(aggregate, true); err != nil {
		return err
	}

	var id, aType string = aggregate.GetID(), aggregate.GetType()
	version := 0

	serializer, err := repository.serializerFor(aType)
	if err != nil {
		return err
	}

	snapshot, err := repository.storage.GetSnapshotByID(aType, id)
	if err != nil {
		return err
	}

	if snapshot != nil {
		serializer.UnmarshalAggregate(snapshot, aggregate)
		version = snapshot.Version
	}

	history, err := repository.storage.GetEventsByID(aType, id, version)
	if err != nil {
		return err
	}

	committed := make([]domain.Event, len(history))

	for i, mEvent := range history {
		event, err := serializer.UnmarshalEvent(&mEvent)
		if err != nil {
			return err
		}

		committed[i] = event
	}

	for _, event := range committed {
		if err := aggregate.Apply(event); err != nil {
			return err
		}
	}

	aggregate.SetVersion(version + len(committed))

	return nil
}

// Store ... Stores Pending Uncommitted Events from the supplied Aggregate
func (repository *Repository) Store(aggregate domain.Storable) error {
	if err := validateAggregate(aggregate, false); err != nil {
		return err
	}

	var id, aType string = aggregate.GetID(), aggregate.GetType()

	work := aggregate.Log()
	uncommitted := make([]domain.EventWrapper, len(work))

	serializer, err := repository.serializerFor(aType)
	if err != nil {
		return err
	}

	for i, event := range work {
		mEvent, err := serializer.MarshalEvent(event)
		if err != nil {
			return err
		}
		uncommitted[i] = *mEvent
	}

	version := aggregate.GetVersion()

	if err := repository.storage.StoreEventsFor(aType, id, version, uncommitted); err != nil {
		return err
	}

	aggregate.ResetLog()
	aggregate.SetVersion(version + len(work))

	return nil
}

// Snapshot ... Stores and Commits the Snapshot
func (repository *Repository) Snapshot(aggregate domain.Snapshotable) error {
	if err := validateAggregate(aggregate, true); err != nil {
		return err
	}

	if len(aggregate.Log()) > 0 {
		return errors.New("You Have uncommitted events")
	}

	var id, aType string = aggregate.GetID(), aggregate.GetType()
	version := aggregate.GetVersion()

	serializer, err := repository.serializerFor(aType)
	if err != nil {
		return err
	}

	snapshot, err := serializer.MarshalAggregate(aggregate)
	if err != nil {
		return err
	}

	if err := repository.storage.StoreSnapshotFor(aType, id, version, snapshot); err != nil {
		return err
	}

	return nil
}
