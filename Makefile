coverage:
	./sbin/coverage.sh
test:
	go test -v
integration:
	gitlab-ci-multi-runner-darwin-amd64 exec docker test-eventstore