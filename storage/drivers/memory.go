package drivers

import (
	"errors"
	"sync"

	"gitlab.com/mandalore/eventstore/domain"
)

type auid struct {
	ID   string
	Type string
}

type snapshot struct {
	Version       int
	Serialization string
	RawData       []byte
}

type event struct {
	ID            string
	Type          string
	Version       int
	Serialization string
	RawData       []byte
}

type aggregate struct {
	Version  int
	Snapshot *snapshot
	Events   []event
}

// InMemory ...
type InMemory struct {
	mu        sync.RWMutex
	directory map[auid]*aggregate
}

// NewInMemory ...
func NewInMemory() *InMemory {
	return &InMemory{
		directory: make(map[auid]*aggregate),
	}
}

func generateDirectoryKey(aType string, aID string) auid {
	return auid{
		ID:   aID,
		Type: aType,
	}
}

func (memory *InMemory) get(key auid) (*aggregate, error) {
	if aggr, found := memory.directory[key]; found {
		return aggr, nil
	}

	return nil, errors.New("Not Found")
}

func (memory *InMemory) put(key auid, aggregate *aggregate) error {
	memory.directory[key] = aggregate

	return nil
}

// GetSnapshotByID ...
func (memory *InMemory) GetSnapshotByID(atype string, id string) (*domain.SnapshotWrapper, error) {
	key := generateDirectoryKey(atype, id)

	curr, err := memory.get(key)
	if err != nil {
		return nil, err
	}

	if curr.Snapshot == nil {
		return nil, nil
	}

	return &domain.SnapshotWrapper{
		Payload:       curr.Snapshot.RawData,
		Serialization: curr.Snapshot.Serialization,
		Version:       curr.Snapshot.Version,
	}, nil
}

// GetEventsByID ...
func (memory *InMemory) GetEventsByID(atype string, id string, version int) ([]domain.EventWrapper, error) {
	key := generateDirectoryKey(atype, id)

	curr, err := memory.get(key)
	if err != nil {
		return nil, err
	}

	events := make([]domain.EventWrapper, 0)

	for _, event := range curr.Events[version:] {
		events = append(events, domain.EventWrapper{
			ID:            event.ID,
			Type:          event.Type,
			Version:       event.Version,
			Serialization: event.Serialization,
			Payload:       event.RawData,
		})
	}

	return events, nil
}

// StoreEventsFor ...
func (memory *InMemory) StoreEventsFor(atype string, id string, version int, uncommitted []domain.EventWrapper) error {
	memory.mu.Lock()
	defer memory.mu.Unlock()

	key := generateDirectoryKey(atype, id)

	curr, err := memory.get(key)
	if err != nil && err.Error() != "Not Found" {
		return err
	}

	if curr == nil {
		curr = &aggregate{
			Version: version,
			Events:  make([]event, 0),
		}
	}

	if curr.Version != version {
		return errors.New("Conflict")
	}

	for _, evt := range uncommitted {
		curr.Events = append(curr.Events, event{
			ID:            evt.ID,
			Type:          evt.Type,
			Version:       curr.Version + 1,
			Serialization: evt.Serialization,
			RawData:       evt.Payload,
		})
		curr.Version++
	}

	memory.put(key, curr)

	return nil
}

// StoreSnapshotFor ...
func (memory *InMemory) StoreSnapshotFor(atype string, id string, version int, msnapshot *domain.SnapshotWrapper) error {
	memory.mu.Lock()
	defer memory.mu.Unlock()

	key := generateDirectoryKey(atype, id)

	curr, err := memory.get(key)
	if err != nil {
		if err.Error() == "Not Found" {
			return errors.New("Cannot create snapshot for aggregate without events")
		}
		return err
	}

	if version > curr.Version {
		return errors.New("Conflict")
	}

	curr.Snapshot = &snapshot{
		Version:       version,
		Serialization: msnapshot.Serialization,
		RawData:       msnapshot.Payload,
	}

	memory.put(key, curr)

	return nil
}
