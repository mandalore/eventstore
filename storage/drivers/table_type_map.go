package drivers

import (
	"database/sql"
	"strings"
	"sync"
)

// TableTypeMap ...
type TableTypeMap struct {
	conn   *sql.DB
	mu     sync.Mutex
	table  string
	idcol  string
	cache  map[string]int
	loaded bool
}

// NewTableTypeMap ...
func NewTableTypeMap(conn *sql.DB, table string) *TableTypeMap {
	tmap := &TableTypeMap{
		table: table,
		conn:  conn,
		idcol: strings.TrimSuffix(table, "s") + "_id",
		cache: make(map[string]int),
	}

	return tmap
}

// GetIDFor ...
func (tmap *TableTypeMap) GetIDFor(slug string) (int, error) {
	if !tmap.loaded {
		if err := tmap.loadTypeMap(slug); err != nil {
			return 0, err
		}
	}

	if typeID, found := tmap.cache[slug]; found {
		return typeID, nil
	}

	id, err := tmap.CreateIDFor(slug)
	if err != nil {
		return 0, err
	}

	tmap.cache[slug] = id

	return id, nil
}

// CreateIDFor ...
func (tmap *TableTypeMap) CreateIDFor(slug string) (int, error) {
	tmap.mu.Lock()
	defer tmap.mu.Unlock()

	sql := "INSERT INTO eventstore." + tmap.table
	sql += "(slug)"
	sql += "VALUES($1) RETURNING *"

	rows, err := tmap.conn.Query(sql, slug)
	if err != nil {
		return 0, err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			id   int
			slug string
		)

		if err := rows.Scan(
			&id,
			&slug,
		); err != nil {
			return 0, err
		}

		tmap.cache[slug] = id
	}

	return tmap.cache[slug], nil
}

func (tmap *TableTypeMap) loadTypeMap(slug string) error {
	tmap.mu.Lock()
	defer tmap.mu.Unlock()

	sql := "SELECT " + tmap.idcol + ", slug FROM eventstore." + tmap.table

	rows, err := tmap.conn.Query(sql)
	if err != nil {
		return err
	}

	defer rows.Close()

	for rows.Next() {
		var (
			id   int
			slug string
		)

		if err := rows.Scan(
			&id,
			&slug,
		); err != nil {
			return err
		}

		tmap.cache[slug] = id
	}

	return nil
}
