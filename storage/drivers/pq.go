package drivers

import (
	"database/sql"
	"errors"

	"gitlab.com/mandalore/eventstore/domain"
)

// Pq represents the implementation for a postgres db.
type Pq struct {
	conn     *sql.DB
	tmappers map[string]*TableTypeMap
}

// NewPq creates a new SQL ...
func NewPq(conn *sql.DB) *Pq {
	storage := &Pq{
		conn: conn,
	}

	storage.tmappers = make(map[string]*TableTypeMap)

	storage.tmappers["atypes"] = NewTableTypeMap(conn, "aggregate_types")
	storage.tmappers["etypes"] = NewTableTypeMap(conn, "event_types")
	storage.tmappers["stypes"] = NewTableTypeMap(conn, "serialization_types")

	return storage
}

func (pq *Pq) fetchSnapshot(atypeID int, id string) (*domain.SnapshotWrapper, error) {
	sqlQuery := `
		SELECT DISTINCT ON (s.aggregate_type_id, s.aggregate_id)
			s.aggregate_version,
			st.slug as serialization_type,
			s.data
		FROM eventstore.snapshots s
		JOIN eventstore.serialization_types st ON (s.serialization_type_id = st.serialization_type_id)
		WHERE aggregate_id = $1 AND aggregate_type_id = $2
		ORDER BY s.aggregate_type_id, s.aggregate_id, s.aggregate_version DESC
		LIMIT 1
	`
	row := pq.conn.QueryRow(sqlQuery, id, atypeID)

	snapshot := &domain.SnapshotWrapper{}

	if err := row.Scan(&snapshot.Version, &snapshot.Serialization, &snapshot.Payload); err != nil && err != sql.ErrNoRows {
		return nil, err
	}

	return snapshot, nil
}

func (pq *Pq) fetchEvents(atypeID int, id string, initialVersion int) ([]domain.EventWrapper, error) {
	events := make([]domain.EventWrapper, 0)

	sqlQuery := `
		SELECT
		  e.event_id,
		  et.slug as event_type,
		  e.aggregate_version,
		  st.slug as serialization_type,
		  data
		FROM eventstore.events e
		JOIN eventstore.event_types et ON (e.event_type_id = et.event_type_id)
		JOIN eventstore.serialization_types st ON (e.serialization_type_id = st.serialization_type_id)
		WHERE aggregate_id = $1 AND aggregate_type_id = $2 AND e.aggregate_version > $3
	`
	rows, err := pq.conn.Query(sqlQuery, id, atypeID, initialVersion)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		event := domain.EventWrapper{}

		if err := rows.Scan(
			&event.ID,
			&event.Type,
			&event.Version,
			&event.Serialization,
			&event.Payload,
		); err != nil {
			return nil, err
		}
		events = append(events, event)
	}

	if len(events) == 0 {
		return nil, errors.New("Not Found")
	}

	return events, nil
}

// GetEventsByID ...
func (pq *Pq) GetEventsByID(atype string, id string, version int) ([]domain.EventWrapper, error) {
	atypeID, err := pq.tmappers["atypes"].GetIDFor(atype)
	if err != nil {
		return nil, err
	}

	events, err := pq.fetchEvents(atypeID, id, version)
	if err != nil {
		return nil, err
	}

	return events, nil
}

// GetSnapshotByID ...
func (pq *Pq) GetSnapshotByID(atype string, id string) (*domain.SnapshotWrapper, error) {
	atypeID, err := pq.tmappers["atypes"].GetIDFor(atype)
	if err != nil {
		return nil, err
	}

	snapshot, err := pq.fetchSnapshot(atypeID, id)
	if snapshot == nil {
		return nil, nil
	}

	return snapshot, nil
}

// StoreEventsFor ...
func (pq *Pq) StoreEventsFor(atype string, id string, version int, uncommitted []domain.EventWrapper) (err error) {
	tx, err := pq.conn.Begin()
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			err = tx.Commit()
		}
	}()

	aTypeID, err := pq.tmappers["atypes"].GetIDFor(atype)
	if err != nil {
		return err
	}

	row := tx.QueryRow(`
		SELECT aggregate_version
		FROM eventstore.aggregates
		WHERE aggregate_id = $1 AND aggregate_type_id = $2
		FOR UPDATE NOWAIT
	`, id, aTypeID)

	currentVersion := 0
	if err := row.Scan(&currentVersion); err != nil && err != sql.ErrNoRows {
		return err
	}

	if currentVersion != version {
		return errors.New("Concurrency Exception")
	}

	stmt, err := tx.Prepare(`
		INSERT INTO eventstore.events
		(event_id, event_type_id, aggregate_id, aggregate_type_id, aggregate_version, serialization_type_id, data)
		VALUES ($1,$2,$3,$4,$5,$6,$7)
	`)

	if err != nil {
		return err
	}

	defer stmt.Close()

	for _, event := range uncommitted {
		version++

		eTypeID, err := pq.tmappers["etypes"].GetIDFor(event.Type)
		if err != nil {
			return err
		}

		sTypeID, err := pq.tmappers["stypes"].GetIDFor(event.Serialization)
		if err != nil {
			return err
		}

		if _, err = stmt.Exec(
			event.ID,
			eTypeID,
			id,
			aTypeID,
			version,
			sTypeID,
			event.Payload,
		); err != nil {
			return err
		}
	}

	if currentVersion == 0 {
		_, err = tx.Exec(`
		INSERT INTO eventstore.aggregates
		(aggregate_id, aggregate_type_id, aggregate_version)
		VALUES($1,$2,$3)
	`, id, aTypeID, version)
	} else {
		_, err = tx.Exec(`
		UPDATE eventstore.aggregates
		SET aggregate_version = $1
		WHERE aggregate_id = $2 AND aggregate_type_id = $3
	`, version, id, aTypeID)
	}

	if err != nil {
		return err
	}

	return nil
}

// StoreSnapshotFor ...
func (pq *Pq) StoreSnapshotFor(atype string, id string, version int, snapshot *domain.SnapshotWrapper) error {
	tx, err := pq.conn.Begin()
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			err = tx.Commit()
		}
	}()

	aTypeID, err := pq.tmappers["atypes"].GetIDFor(atype)
	if err != nil {
		return err
	}

	sTypeID, err := pq.tmappers["stypes"].GetIDFor(snapshot.Serialization)
	if err != nil {
		return err
	}

	row := tx.QueryRow(`
		SELECT aggregate_version
		FROM eventstore.aggregates
		WHERE aggregate_id = $1 AND aggregate_type_id = $2
		FOR UPDATE NOWAIT
	`, id, aTypeID)

	currentVersion := 0
	if err := row.Scan(&currentVersion); err != nil && err != sql.ErrNoRows {
		return err
	}

	if currentVersion < version {
		return errors.New("Concurrency Exception")
	}

	if _, err := tx.Exec(`
		INSERT INTO eventstore.snapshots
		(aggregate_id, aggregate_type_id, aggregate_version, serialization_type_id, data)
		VALUES ($1,$2,$3,$4,$5)
	`, id, aTypeID, version, sTypeID, snapshot.Payload); err != nil {
		return err
	}

	return nil
}
