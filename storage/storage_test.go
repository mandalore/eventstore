package storage

import (
	"database/sql"
	"io/ioutil"
	"log"
	"os"
	"strings"
	"testing"

	"gitlab.com/mandalore/eventstore/domain"
	"gitlab.com/mandalore/eventstore/storage/drivers"

	uuid "github.com/satori/go.uuid"

	"fmt"
	"reflect"
)

var aggregateID string
var snap domain.SnapshotWrapper
var firstBatch []domain.EventWrapper
var secondBatch []domain.EventWrapper
var stores []IStorage

func init() {
	aggregateID = uuid.NewV4().String()

	snap = domain.SnapshotWrapper{
		Serialization: "text",
		Payload:       []byte("o znapshot"),
	}

	firstBatch = []domain.EventWrapper{
		domain.EventWrapper{
			ID:            uuid.NewV4().String(),
			Type:          "something_happened",
			Serialization: "text",
			Payload:       []byte("Bullshit"),
		},
	}

	secondBatch = []domain.EventWrapper{
		domain.EventWrapper{
			ID:            uuid.NewV4().String(),
			Type:          "something_else_happened",
			Serialization: "text",
			Payload:       []byte("WOOT"),
		},
		domain.EventWrapper{
			ID:            uuid.NewV4().String(),
			Type:          "something_else_happened",
			Serialization: "text",
			Payload:       []byte("WAAT"),
		},
	}

	stores = []IStorage{
		initInMemoryStorage(),
		initPQStorage(),
	}
}

func initPQStorage() *drivers.Pq {
	host := "localhost"
	if os.Getenv("TEST_ENV") == "gitlab" {
		host = "postgres"
	}

	db, err := sql.Open("postgres", "host="+host+" user=postgres dbname=postgres sslmode=disable")
	if err != nil {
		log.Fatal("Could not connect to the database", err)
	}

	if err := loadFile(db, "../sys/schema/01-bootstrap.sql"); err != nil {
		log.Fatal("Failed to load schema into ", host, "Error:", err.Error())
	}

	return drivers.NewPq(db)
}

func initInMemoryStorage() *drivers.InMemory {
	return drivers.NewInMemory()
}

func loadFile(db *sql.DB, fileName string) (err error) {
	file, err := ioutil.ReadFile(fileName)

	if err != nil {
		log.Fatal(err)
	}

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	defer func() {
		if err != nil {
			tx.Rollback()
			return
		}
		err = tx.Commit()
	}()

	requests := strings.Split(string(file), ";")

	for _, request := range requests {
		_, err = db.Exec(request)
		if err != nil {
			return
		}
	}

	return
}

func TestStorage(t *testing.T) {
	for _, stor := range stores {
		storType := fmt.Sprintf("%T", stor)

		err := stor.StoreEventsFor("test_stream", aggregateID, 0, firstBatch)
		if err != nil {
			t.Error(storType, "Could not Store simple Event slice", err.Error())
		}

		err = stor.StoreEventsFor("test_stream", aggregateID, 0, firstBatch)
		if err == nil {
			t.Error(storType, "Managed to Store Conflicting Event", err.Error())
		}

		err = stor.StoreEventsFor("test_stream", aggregateID, 10, firstBatch)
		if err == nil {
			t.Error(storType, "Managed to Store Future Event", err.Error())
		}

		err = stor.StoreEventsFor("test_stream", aggregateID, 1, secondBatch)
		if err != nil {
			t.Error(storType, "Could not Store simple Event slice appending on correct version", err.Error())
		}

		events, err := stor.GetEventsByID("missing_stream", aggregateID, 0)
		if err == nil {
			t.Error(storType, "Found existing aggregate ID on a string it should not be in")
		}

		events, err = stor.GetEventsByID("test_stream", "missing_id", 0)
		if err == nil {
			t.Error(storType, "Found missing aggregate ID on a string it should not be in")
		}

		events, err = stor.GetEventsByID("test_stream", aggregateID, 0)
		if err != nil {
			t.Error(storType, "Failed to find existing aggregate", err.Error())
		}

		expected := firstBatch[:]
		expected = append(expected, secondBatch...)
		actual := events

		for i, sEvent := range expected {
			if sEvent.ID != actual[i].ID {
				t.Error(storType, "Fetched ID does not match with stored ID")
			}
			if sEvent.Type != actual[i].Type {
				t.Error(storType, "Fetched Type does not match with stored Type", sEvent.Type, "!=", actual[i].Type)
			}
			if sEvent.Serialization != actual[i].Serialization {
				t.Error(storType, "Fetched Serialization does not match with stored Serialization", sEvent.Serialization, "!=", actual[i].Serialization)
			}
			if !reflect.DeepEqual(sEvent.Payload, actual[i].Payload) {
				t.Error(storType, "Fetched Serialization does not match with stored Serialization", sEvent.Payload, "!=", actual[i].Payload)
			}
		}

		if events[0].Version != 1 {
			t.Error(storType, "Returned Version is not correct", events[0].Version)
		}

		if len(events) != 3 {
			t.Error(storType, "Returned incorrect number of events")
		}

		err = stor.StoreSnapshotFor("test_stream", aggregateID, 20, &snap)
		if err == nil {
			t.Error(storType, "Manage to store snapshot in the future", err.Error())
		}

		err = stor.StoreSnapshotFor("test_stream", aggregateID, 2, &snap)
		if err != nil {
			t.Error(storType, "Failed to store snapshot", err.Error())
		}

		snapshot, err := stor.GetSnapshotByID("test_stream", aggregateID)
		if err != nil {
			t.Error(storType, "Failed to find existing aggregate", err.Error())
		}

		if snapshot.Version != 2 {
			t.Error(storType, "Returned Initial Version is not correct, should be 2, got ", snapshot.Version)
		}

		events, err = stor.GetEventsByID("test_stream", aggregateID, snapshot.Version)
		if err != nil {
			t.Error(storType, "Failed to find existing aggregate", err.Error())
		}

		if events[0].Version != 3 {
			t.Error(storType, "Returned Version is not correct", events[0].Version)
		}

		if len(events) != 1 {
			t.Error(storType, "Returned incorrect number of events", len(events))
		}
	}
}
