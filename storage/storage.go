package storage

import "gitlab.com/mandalore/eventstore/domain"

// IStorage ...
type IStorage interface {
	GetEventsByID(atype string, id string, version int) ([]domain.EventWrapper, error)
	GetSnapshotByID(atype string, id string) (*domain.SnapshotWrapper, error)
	StoreEventsFor(atype string, id string, expected int, events []domain.EventWrapper) error
	StoreSnapshotFor(atype string, id string, expected int, snapshot *domain.SnapshotWrapper) error
}
